# [jms](https://phppackages.org/s/jms)/[serializer](https://phppackages.org/p/jms/serializer)

Library for (de-)serializing data of any complexity (supports XML, JSON, YAML) http://jmsyst.com/libs/serializer

(Unofficial demo and howto)

* [serializer](https://phppackages.org/s/serializer)@phppackages
* symfony/[serializer](https://phppackages.org/p/symfony/serializer)
* friendsofsymfony/[rest-bundle](https://phppackages.org/p/friendsofsymfony/rest-bundle)
* [What's new in jms/serializer v2.0](https://www.goetas.com/blog/whats-new-in-jmsserializer-v20/)
* [*The Problem with JsonSerializable and Doctrine when using Symfony*
  ](https://theiconic.tech/the-problem-with-jsonserializable-and-doctrine-when-using-symfony-ad760e986b04?gi=1300330197a4)
  2020-06 Nootan Ghimire